# Flaskr

This is the [Flaskr tutorial](http://flask.pocoo.org/docs/0.12/tutorial/) completed with some basic testing.

I was following v0.12 of Flask at the time, but the tests incorporate some stuff from the later Flask Guide which appears to be pulled from the upcoming 1.0 milestone.

Make sure to compare against flask/examples/flaskr from the 0.12 tag/branch.
