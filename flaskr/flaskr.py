import os
import sqlite3

from flask import Flask
from flask import abort
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import Response
from flask import request
from flask import session
from flask import url_for
from redis import Redis
import time
from datetime import datetime

ONLINE_LAST_MINUTES = 5

app = Flask(__name__)  # create the app instance
app.config.from_object(__name__)  # load config from this file, flaskr.py

#load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'flaskr.db'),
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

# establish default redis connection
redis = Redis()


# Database stuff
def connect_db():
    """Connects to the specific database"""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def init_db():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()


@app.cli.command('initdb')
def initdb_command():
    """Initializes the database."""
    init_db()
    print('Initialized the database.')


def get_db():
    """Opens a new db connection if none; otherwise returns it"""
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the db at the end of the request"""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


# Helper functions
def mark_online(user_id):
    now = int(time.time())
    expires = now + (ONLINE_LAST_MINUTES * 60) + 10
    all_users_key = 'online-users/%d' % (now // 60)
    user_key = 'user-activity/%s' % user_id
    p = redis.pipeline()
    p.sadd(all_users_key, user_id)
    p.set(user_key, now)
    p.expireat(all_users_key, expires)
    p.expireat(user_key, expires)
    p.execute()


def get_online_users():
    current = int(time.time()) // 60
    minutes = xrange(ONLINE_LAST_MINUTES)
    return redis.sunion(['online-users/%d' % (current - x)
                         for x in minutes])


# Callbacks
@app.before_request
def mark_current_user_online():
    mark_online(request.remote_addr)


# Routes begin
@app.route('/')
def show_entries():
    mark_online(request.remote_addr)
    db = get_db()
    cur = db.execute('select title, text from entries order by id desc')
    entries = cur.fetchall()
    data = get_online_users()
    if data is None:
        online_users = "None"
    elif len(data) == 0:
        online_users = "0"
    else:
        online_users = ', '.join(data)
    return render_template('show_entries.html',
                           entries=entries,
                           online_users=online_users)


@app.route('/add', methods=['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    db = get_db()
    db.execute('insert into entries (title, text) values(?, ?)',
               [request.form['title'], request.form['text']])
    db.commit()
    flash('New entry was successfully posted', category='message')
    return redirect(url_for('show_entries'), code=302, Response=None)


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in', category='message')
            return redirect(url_for('show_entries'), code=302, Response=None)
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out', category='message')
    return redirect(url_for('show_entries'), code=302, Response=None)
